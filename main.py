from pydantic import BaseModel, Field
from enum import Enum


from fastapi import FastAPI
from fastapi import Body, Query, Path
from fastapi import status

app = FastAPI()


#Models
class HairColor(Enum):
    black="Black"
    blondie="blondie"
    brown="Brown"
    blue="Blue"

class PersonBase(BaseModel):
    fist_name: str=Field(
        ...,
        min_length=2,
        max_length=50,
        example="Goku" 
    )
    last_name: str=Field(
        ...,
        min_length=2,
        max_length=50,
        example="Son"
    )
    age: int=Field(
        ...,
        gt=0,
        le=100,
        example=40
    )
    hair_color: HairColor=Field(
        default = None,
        example = HairColor.black
    )
    is_married:bool=Field(default=False)
   
class PersonOut(PersonBase):
     password: str=Field(
        ...,
        min_length=8
    )

class Person(PersonBase):
    pass




@app.post(path="/person/new")
def new_person(person:PersonOut=Body(...)):
    return person


@app.get(path="/",status_code=status.HTTP_200_OK)
def home():
    return {"Hello":"Vegetta"}