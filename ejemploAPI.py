from typing import Optional 
from pydantic import BaseModel, Field
from enum import Enum


from fastapi import FastAPI
from fastapi import Body, Query, Path

app = FastAPI()

#Models
class Role(Enum):
    dev="Developer"
    test="Tester"
    sec="Security"
    mng="Manager"



class User(BaseModel):
    user_id:int = Field(
        ...,
        gt=0
    )
    name:str=Field(
        ...,
        min_length=2,
        max_length=50,
    )
    last_name:str=Field(
        ...,
        min_length=2,
        max_length=50,
    )

    age:int
    role:Role


class Article(BaseModel):
    article_name: str
    #article_description: str
    article_sugar: int
    #article_available: bool
    #article_color_chocolate:Optional[str] = None




@app.get("/")
def home():
    return {"student":"david_avilac"}



@app.post("/article/new")
def create_article(article: Article = Body(...)):
    return article

@app.get("/user/new")
def new_user(
    name:str=Query(
        ...,
        min_length=2,
        max_length=50
        ),
    age:int=Query(...)
):
    return {name:age}

@app.get("/user/detail/")
def user_detrail(
    user_id:int=Path(
        ...,
        gt=0,
        title="User ID",
        description="Numero de identidad."
    )

):
    return {"User ID":user_id}

@app.get("/user/update/{user_id}")
def update_user(
    user_id:int=Path(
        ...,
        gt=0,
        title="User ID",
        description="Numero de identidad."
    ),
    user:User=Body(...)
):
    return {user_id:"OK"}